import Vue from 'vue'
import App from './App.vue'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
Vue.config.productionTip = false
Vue.directive('testdirective', {
    bind: function(el, binding) {
        el.innerHTML = binding.value;
        //argent through
        // if (binding.arg === 'green') {
        //     el.style.color = "green";
        // }
        // if (binding.arg === 'red') {
        //     el.style.color = "red";
        // }
        if (binding.modifiers.green) {
            el.style.color = "green";
        }
        if (binding.modifiers.small) {
            el.style.fontSize = "20px";
        }
        if (binding.modifiers.red) {
            el.style.color = "red";
        }
        if (binding.modifiers.big) {
            el.style.fontSize = "45px";
        }

    }
})
new Vue({
    render: h => h(App),
}).$mount('#app')